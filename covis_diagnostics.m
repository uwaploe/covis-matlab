%function [covis] = covis_sweep_diagnostics(swp_path, swp_name, json_file)
%
% Covis diagnosics
%
% ----------
% This program is free software distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY. You can redistribute it and/or modify it.
% Any modifications of the original software must be distributed in such a 
% manner as to avoid any confusion with the original work.
% 
% Please acknowledge the use of this software in any publications arising  
% from research that uses it.
% 
% ----------
% Version 1.0 - cjones@apl.washington.edu 09/2010
%
clear all;

Verbose = 1;
TCM_Plot = 0;
Scan_Plot_2D = 1;
Scan_Plot_3D = 0;
Elem_Plot = 1;
Pause_Plot = 1;

%Adjust_Position = 0;

colormap(jet);

swp_name = 0;
swp_path = 0;
json_file = 0;

if(swp_path == 0) swp_path = []; end
if(swp_name == 0) swp_name = []; end
if(json_file == 0) json_file = []; end
json_path = 'input';

% pick a sweep archive, if none given
if(isempty(swp_name)) 
   [swp_name, swp_path] = uigetfile(fullfile(swp_path,'*.zip'), 'Pick a COVIS Sweep Archive');
   if(swp_name == 0) 
      covis = [];
      return; 
   end;
end

% pick a json file, if none given
if(isempty(json_file)) 
   [json_file, json_path] = uigetfile(fullfile('input','*.json'), 'Pick a COVIS JSON file');
   if(json_file == 0) 
      covis = [];
      return; 
   end;
end

% if a zip file is given, and it archive dir doesn't exist, 
% unzip the sweep archive
[pathstr, name, ext] = fileparts(fullfile(swp_path,swp_name));
if(strcmp(ext,'.zip'))
    % unzip file, if doesn't exist
    if(~exist(fullfile(pathstr,name)))
        files = unzip(fullfile(swp_path, swp_name), pathstr);
        % check if zip file has been renamed
        swp_dir = fileparts(files{1});
        if ~strcmp(swp_dir, fullfile(pathstr,name))
            movefile(swp_dir, fullfile(pathstr,name));
        end
    end
    swp_name = name;
end

% parse sweep.json file in data archive
swp_file = 'sweep.json';
json_str = fileread(fullfile(swp_path, swp_name, swp_file));
swp = parse_json(json_str);

% set sweep path and name
swp.path = swp_path;
swp.name = swp_name;

% parsing the .json file
%  which contain all the user supplied parameters
json_str = fileread(fullfile(json_path, json_file));
covis = parse_json(json_str);

% define the data grid
%[covis.grid] = covis_rectgrid(covis.grid);
%covis.grid.name = swp.name;

grd = covis.grid;
usr = covis.user;
pos = covis.sonar.position;
dsp = covis.processing;
bfm = covis.processing.beamformer;
cal = covis.processing.calibrate;
filt = covis.processing.filter;

scrsz = get(0,'ScreenSize');

alt = pos.altitude;
origin = [0,0,alt]; % sonar is always at (0,0,0) in world coords

% directory list of *.bin file
swp_dir = fullfile(swp_path, swp_name);
file = dir(fullfile(swp_dir, '*.bin'));
nfiles = length(file);

% Read index file
% The index file contains the sweep parameters:
% ping,seconds,microseconds,pitch,roll,yaw,kPAngle,kRAngle,kHeading
% in csv format
ind_file = 'index.csv';
% should check if file exists
if(Verbose)
    fprintf('Parsing %s\n', fullfile(swp_dir, ind_file));
end;
csv = csvread(fullfile(swp_dir, ind_file), 1, 0);
if(size(csv,1) ~= nfiles)
    error('index size and file number mismatch');
end

% save index data in ping structure
for n=1:nfiles
    png(n).num = csv(n,1);
    png(n).sec = (csv(n,2) + csv(n,3)/1e6);
    % save rotator angles
    png(n).rot.pitch = csv(n,4);
    png(n).rot.roll = csv(n,5);
    png(n).rot.yaw = csv(n,6)';
    % save tcm6 angles
    png(n).tcm.kPAngle = csv(n,7)';
    png(n).tcm.kRAngle = csv(n,8)';
    png(n).tcm.kHeading = pos.declination + csv(n,9)';
end


% loop over ping files
for n=1:nfiles
    
    fign = 1;
    
    % parse filenames
    bin_file = file(n).name;
    [type,ping_num] = strread(bin_file,'rec_%d_%d.bin');
    json_file = sprintf('rec_7000_%06d.json',ping_num);
    
    %if(Verbose)
    %    fprintf('Parsing %s\n', fullfile(swp_path, json_file));
    %end;
    
    % read ping meta data from fson file
    json_str = fileread(fullfile(swp_dir, json_file));
    json = parse_json(json_str);
    png(n).hdr = json.hdr;
    
    if(Verbose)
        fprintf('Reading %s\n', fullfile(swp_dir, bin_file));
    end;
    
    % read raw element quadrature data
    [hdr, data] = covis_read(fullfile(swp_dir, bin_file));
    
    % define beamformer parameters
    bfm.fc = png(n).hdr.xmit_freq;
    bfm.c = png(n).hdr.sound_speed;
    bfm.fs = png(n).hdr.sample_rate;
    bfm.first_samp = hdr.first_samp + 1;
    bfm.last_samp = hdr.last_samp;

    % define sonar attitude
    elev = (pi/180) * png(n).tcm.kPAngle;
    roll = (pi/180) * png(n).tcm.kRAngle;
    yaw = (pi/180) * png(n).tcm.kHeading;
    
    str = sprintf('Ping Number %d, Elevation %.2f, Roll %.2f, Yaw %.2f\n', ...
            png(n).hdr.ping_num, elev*180/pi, roll*180/pi, yaw*180/pi);

    [data, filt, png(n)] = covis_filter(data, filt, png(n));
    
    if(Verbose)
        fprintf('%s\n',str);
    end;
    
    if(Elem_Plot)
        h = figure(fign);  fign = fign+1; clf;
        subplot(4,1,1:3);
        imagesc(abs(data));
        set(gca,'YDir','normal')
        %surf(xv,yv,zv,(abs(bf_sig).^2)); shading flat; axis equal;
        %str = sprintf('Ping Number %d, Elevation %.2f, Roll %.2f, Yaw %.2f\n', ...
        %    png(n).hdr.ping_num, elev*180/pi, roll*180/pi, yaw*180/pi);
        title({'Magnatude of Element data'; str});
        ylabel('Sample number');
        %xlabel('Element Number');
        colorbar
        %set(h, 'Position',[1 scrsz(4)/2 scrsz(3)/2 scrsz(4)/2]);
        subplot(4,1,4);
        sec = [png.sec] - png(1).sec;
        now = png(n).sec - png(1).sec;
        tcm = [png.tcm];
        elev = [tcm.kPAngle];
        plot(sec, elev, '.', [now now],[-62 62],'r');
        axis([sec(1) sec(end) -62 62]);
        title(str);
        title('TCM6 Elevation');
        ylabel('Degrees');
        xlabel('Seconds');
        refresh;
        %pause;
    end

    if(Scan_Plot_2D)
        [bfm, bf_sig] = covis_beamform(bfm, data); % beamform the quadrature data
        bf_sig = covis_calibration(bf_sig, bfm, png(n), cal);
        % transform sonar coords into world coords
        range = bfm.range;
        azim = bfm.angle;
        h = figure(fign); clf; fign = fign+1;
        subplot(4,1,1:3);
        elev = (pi/180) * png(n).tcm.kPAngle;
        roll = (pi/180) * png(n).tcm.kRAngle;
        yaw = (pi/180) * png(n).tcm.kHeading;
        [xv, yv, zv] = covis_coords(origin, range, azim, yaw, 0, 0);
        pcolor(xv,yv,10*log10(abs(bf_sig).^2)); shading flat; axis equal;
        %surf(xv,yv,zv,(abs(bf_sig).^2)); shading flat; axis equal;
        %str = sprintf('Ping Number %d, Elevation %.2f, Roll %.2f, Yaw %.2f\n', ...
        %    png(n).hdr.ping_num, elev*180/pi, roll*180/pi, yaw*180/pi);
        title({'Scan Intensity [db]'; str});
        colorbar
        %figsz = get(h, 'OuterPosition');
        %set(h, 'Position',[scrsz(3)/2 scrsz(4)/2 scrsz(3)/2 scrsz(4)/2]); 
        subplot(4,1,4);
        sec = [png.sec] - png(1).sec;
        now = png(n).sec - png(1).sec;
        tcm = [png.tcm];
        elev = [tcm.kPAngle];
        plot(sec, elev, '.', [now now],[-62 62],'r');
        axis([sec(1) sec(end) -62 62]);
        title(str);
        title('TCM6 Elevation');
        ylabel('Degrees');
        xlabel('Seconds');
        refresh;
    end

    if(Scan_Plot_3D)

        [bfm, bf_sig] = covis_beamform(bfm, data); % beamform the quadrature data
        bf_sig = covis_calibration(bf_sig, bfm, png(n), cal_mode); 
        % transform sonar coords into world coords
        range = bfm.range;
        azim = bfm.angle;
        h = figure(fign); [az,el] = view; clf;
        elev = (pi/180) * png(n).tcm.kPAngle;
        roll = (pi/180) * png(n).tcm.kRAngle;
        yaw = (pi/180) * png(n).tcm.kHeading;
        [xv, yv, zv] = covis_coords(origin, range, azim, yaw, roll, elev);

        surf(xv,yv,zv,log10(abs(bf_sig).^2)); shading flat; axis equal; 
        % plot the bathymetry map
        hold on
        axis equal;
        str = sprintf('Ping Number %d: Elevation %.2f, Roll %.2f, Yaw %.2f\n', ...
            png(n).hdr.ping_num, elev*180/pi, roll*180/pi, yaw*180/pi);
        title(str);
        %colorbar
        refresh;
        dx = 0.5; dy = 0.5;
        view(az,el);
        [az,el] = view;
        fign = fign+1;
        %pause;
    end;
    
    if(Pause_Plot) 
        go = input('Hit Enter to continue (q to quit): ','s');
        if(go == 'q') break; end
    end

end       % End loop on ping number


% save local copies of covis structs
covis.sweep = swp;
covis.beamformer = bfm;
covis.ping = png;

% Make diagnostic plots

if(TCM_Plot)
    
    sec = [covis.ping.sec];
    sec = sec - sec(1);
    
    figure(fign); clf; fign = fign+1;
    subplot(2,1,1);
    rot = [covis.ping.rot];
    pitch = [rot.pitch];
    roll = [rot.roll];
    yaw = [rot.yaw];
    plot(sec, pitch, '.', sec, roll, '.', sec, yaw, '.');
    legend('Pitch','Roll','Yaw','Location','Best');
    title('Rotator Angles');
    ylabel('Degrees');
    
    subplot(2,1,2);
    tcm = [covis.ping.tcm];
    elev = [tcm.kPAngle];
    roll = [tcm.kRAngle];
    compass = [tcm.kHeading];
    plot(sec, elev, '.', sec, roll, '.', sec, compass, '.');
    legend('Elev','Roll','Heading','Location','Best');
    title('TCM6 Angles');
    ylabel('Degrees');
    xlabel('Seconds');

end

%end

