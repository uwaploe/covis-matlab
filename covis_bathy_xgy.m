% this function is used to reframe the bathymetry of Grotto using the
% bottom of COVIS as the origin.

function [xb,yb,zb,depth_covis] = covis_bathy_xgy(covis,type)
% input: 
% covis: covis structure array
% type: 'diffuse flow', 'imaging', 'doppler'
if strcmp(type,'imaging')
    grd = covis.grid;
else
    grd = covis.grid{1};
end

% correct for the movement of COVIS on June 20th,2013
date_unix = covis.sweep.timestamp{1};
date_mat = unixtime2mat(date_unix);
if date_mat > datenum(2013,6,20,0,0,0);
    x0 = -2.25;
    y0 = 0.26;
else
    x0 = 0;
    y0 = 0;
end

% translate and rotate bathy to fit covis data
% these are set by manually trans and rotating the bathy
coff = 12.0 * (pi/180); % compass offset
xoff = -1;
yoff = 2;
zoff = 1;

% load bathymetry data
file_path = 'C:\COVIS\bathymetry\grotto';
file_name =  'covis_bathy_1.mat'; % multi-beam data collected in 2010
load(file_name);
if(isfield(covis.sonar.position, 'easting'))
    utm_x0 = covis.sonar.position.easting;
    utm_y0 = covis.sonar.position.northing;
else
    fprintf('No sonar positions available\n');
    return;
end

if(isfield(covis.sonar.position, 'depth'))&&x0==0&&y0==0
    depth_covis = covis.sonar.position.depth;
else
    F = TriScatteredInterp(utm_x(:),utm_y(:),depth(:));
    depth_covis = F(utm_x0+x0,utm_y0+y0);
end
x = utm_x - (utm_x0+x0);
y = utm_y - (utm_y0+y0);
z = depth-depth_covis; % height above the bottom of COVIS (m)

% correct for the uncertainty in the bathymetry data
R = sqrt(x.^2 + y.^2);
theta = atan2(y, x) + coff;
x = R.*cos(theta) + xoff;
y = R.*sin(theta) + yoff;
z = z + zoff;

%rotate(hb,[0,0,1],theta_off,[0,0,0]);
%rotate(hc,[0,0,1],theta_off,[0,0,0]);

% resample bathy onto a uniform rectangular grid
% this is necessary because utm_x, and utm_y are not necessarily uniform
xmin = grd.bounds.xmin;
xmax = grd.bounds.xmax;
ymin = grd.bounds.ymin;
ymax = grd.bounds.ymax;
dx = grd.spacing.dx;
dy = grd.spacing.dy;

xb = xmin:dx:xmax;
yb = ymin:dy:ymax;
% xb = -40:0.1:10;
% yb = -40:0.1:10;
[xb,yb] = meshgrid(xb,yb);
F = TriScatteredInterp(x(:),y(:),z(:));
zb = F(xb,yb); % gridded height above the bottom of COVIS (m)
end

