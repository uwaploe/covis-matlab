%
% test matlab script
% using matlab in batch mode
% matlab needs to be started using the -nodisplay mode, 
% matlab -nojvm doesn't work because tar and zip needs java.
%


filename = '/nfs/covis-nas/Data/2011/10/24/APLUWCOVISMBSONAR001_20111024T000038.402Z-IMAGING.tar.gz';
outputdir = '/nfs/covis-nas/Data/decimated/2011/10/24';

% extracts the data into 'outputdir'
% decimates the data and saves it in a new archive 
% call APLUWCOVISMBSONAR001_20111024T000038.402Z-IMAGING-8K.zip

covis_decimate(filename, outputdir);

% process sweep image

swp_path='/nfs/covis-nas/Data/decimated/2011/10/24';
swp_name='APLUWCOVISMBSONAR001_20111024T000038.402Z-IMAGING-8K';
json_file = 'covis_image.json';

covis = covis_imaging_sweep(swp_path, swp_name, json_file);



